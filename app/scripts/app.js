'use strict';

/**
 * @ngdoc overview
 * @name madain
 * @description
 * # madain Client App
 *
 * Main module of the application.
 */

var currentEnv = "staging";
var settings = {
    endpoints: {
        "dev": "http://localhost:3000",
        "staging": "https://madain-server.herokuapp.com",
        "production": ""
    },
    pubnubKeys: {
        "dev": {
            "pubKey" : "pub-c-5af34070-5a04-4863-bc30-4c730195ebbe",
            "subKey" : "sub-c-ae581a56-d874-11e4-b029-02ee2ddab7fe"
        },
        "staging" : {
            "pubKey" : "pub-c-5af34070-5a04-4863-bc30-4c730195ebbe",
            "subKey" : "sub-c-ae581a56-d874-11e4-b029-02ee2ddab7fe"
        },
        "production" : {}
    },

    endpoint: function() {
        return settings.endpoints[currentEnv];
    },

    pubnubKey: function(keyType) {
        return settings.pubnubKeys[currentEnv][keyType];
    }
};

var madain = angular.module('madain', [
    'ui.bootstrap',
    'ngAnimate',
    'angular-loading-bar',
    'ngResource',
    'ng-token-auth',
    'permission',
    'ui.router',
    'pubnub.angular.service',
]);