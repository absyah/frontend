madain
.config(['$stateProvider', '$urlRouterProvider', '$authProvider',
function($stateProvider, $urlRouterProvider, $authProvider) {

    // Auth end-point
    $authProvider.configure({
        apiUrl: settings.endpoint()
    });

    // For any unmatched url, redirect to /
    $urlRouterProvider.otherwise(function($injector){
        var $state = $injector.get('$state');
        $state.go("login");
    });

    // Now set up the states
    $stateProvider

    .state("landing", {
        templateUrl: "views/common/landing.html"
    })

    .state("landing.welcome", {
        url: "/welcome",
        templateUrl: "views/static/welcome.html",
    })

    .state("landing.about", {
        url: "/about",
        templateUrl: "views/static/about.html",
    })

    .state("main", {
        templateUrl: "views/common/container.html"
    })

    .state("main.dashboard", {
        url: "/dashboard",
        templateUrl: "views/dashboard/index.html",
        controller: "DashboardIndexController",
        data: {
            permissions: {
                only: ['user'],
                redirectTo: 'login'
            }
        }
    })

    .state("login", {
        url: "/login",
        templateUrl: "views/auth/login.html",
        controller: "LoginController",
        data: {
            permissions: {
                except: ['user'],
                redirectTo: 'main.dashboard'
            }
        }
    })

    .state("signUp", {
        url: "/signup",
        templateUrl: "views/auth/registration.html",
        controller: "RegistrationController",
        data: {
            permissions: {
                except: ['user'],
                redirectTo: 'main.dashboard'
            }
        }
    })

}])

.run(function(Permission, $auth, $rootScope) {

    Permission.defineRole('user', function(stateParams) {
        var userState = $auth.validateUser().$$state.status;

        switch (userState) {
            case 0: // No changed with user's token
            case 1: // New user token
                return true;
            case 2: // no credential provided
                return false;
        }

        return false;
    });

})
