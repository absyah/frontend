madain.controller('LoginController', ['$scope', '$auth', '$state',
    function($scope, $auth, $state) {

        $scope.$on('auth:login-success', function() {
            console.log('logged in');
        });

        $scope.submitLogin = function() {
            var userParams = {
                email: $scope.loginForm.email,
                password: $scope.loginForm.password,
            };

            $auth.submitLogin(userParams)
            .then(function(data) {
                $state.go('main.dashboard');
            })
            .catch(function(error) {
                console.log('error');
            });
        };
    }
]);