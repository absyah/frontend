madain.controller('RegistrationController', ['$scope', '$auth', '$state',
    function($scope, $auth, $state) {

        $scope.$on('auth:registration-email-success', function(ev, message) {
            console.log('registered')
        });

        $scope.createAccount = function() {

            var userParams = {
                email: $scope.registrationForm.email,
                password: $scope.registrationForm.password,
                confirm_success_url: settings.endpoint()
            };

            $auth.submitRegistration(userParams)
            .then(function(data) {
                $state.go('login');
            })
            .catch(function(error) {
                console.log(error)
            });
        };
    }
]);