madain.controller('SignOutController', ['$scope', '$auth', '$state',
    function($scope, $auth, $state) {

        $scope.$on('auth:logout-success', function(ev) {
            console.log('Logged Out!');
        });

        $scope.signOut = function() {
            $auth.signOut()
            .then(function(data) {
                $state.go('login');
            })
            .catch(function(error) {
                console.log('failed')
            });
        };
    }
]);