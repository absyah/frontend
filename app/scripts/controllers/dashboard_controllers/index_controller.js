madain.controller('DashboardIndexController', ['$rootScope','$scope', 'PubNub', '$state', '$auth',
    function($rootScope, $scope, PubNub, $state, $auth) {
        // http://www.pubnub.com/blog/angularjs-101-from-zero-to-angular-in-seconds/
        $scope.userEmail = $rootScope.user.email;
        $scope.uuid = $rootScope.user.id;
        $scope.channel = "Dashboard Channel";
        $scope.messages = ['Welcome to ' + $scope.channel];

        if (!$rootScope.initialized) { 
            // Initialize the PubNub service
            PubNub.init({
                subscribe_key : settings.pubnubKey('subKey'),
                publish_key   : settings.pubnubKey('pubKey'),
                uuid          : $scope.uuid,
                ssl           : true
            });
            $rootScope.initialized = true; 
        }

        // Subscribe to the Channel
        PubNub.ngSubscribe({ channel: $scope.channel });

        // Create a publish() function in the scope 
        $scope.publish = function() {
            PubNub.ngPublish({
                channel: $scope.channel,
                message: "[" + $scope.userEmail + "] " + $scope.newMessage
            });
            $scope.newMessage = '';
        };

        // Register for message events 
        $rootScope.$on(PubNub.ngMsgEv($scope.channel), function(ngEvent, payload) {
            $scope.$apply(function() {
                $scope.messages.push(payload.message);
            });
        });
    }
]);