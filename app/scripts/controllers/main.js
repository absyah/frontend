'use strict';

/**
 * @ngdoc function
 * @name madain.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the madain
 */
madain.controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
